Data supporting the findings reported in paper: [https://doi.org/10.3390/ijms232113514](https://doi.org/10.3390/ijms232113514)

The Importance of Charge Transfer and Solvent Screening in the Interactions of Backbones and Functional Groups in Amino Acid Residues and Nucleotides

Vladimir Sladek and Dmitri G. Fedorov

Int. J. Mol. Sci. 2022, 23(21), 13514
